#import "JMFNameEntry.h"


@interface JMFNameEntry ()

+(NSArray *) observableKeys;

@end


@implementation JMFNameEntry

//+(NSArray *)observableKeys {
//    return @[JMFNameEntryAttributes.name,
//             JMFNameEntryAttributes.creationDate];
//
//    
//}


#pragma mark - Lifecycle
-(void)awakeFromInsert{
    [super awakeFromInsert];
    
    [self setupKVO];
}

-(void)awakeFromFetch {
    
    [super awakeFromFetch];
    
    [self setupKVO];
}


-(void) willTurnIntoFault {
    [super willTurnIntoFault];
    
    [self tearDownKVO];
}

/*
 * Implementarlo aqui evita tener que repetirlo 
 * porque 'Notebook' y 'Note' heredan de 'NameEntry'.
 *
 */

#pragma mark - KVO

// Alta
-(void) setupKVO {
//    
//    NSArray *keys = @[JMFNameEntryAttributes.name,
//                      JMFNameEntryAttributes.creationDate];
    
//    for (NSString *key in [JMFNameEntry observableKeys]) {
    for (NSString *key in [self.class observableKeys]) {  // Para que escuche la clase adecuada: 'Notebook' y 'Note'
        [self addObserver:self
               forKeyPath:key
                  options:NSKeyValueObservingOptionNew |
                  NSKeyValueObservingOptionOld
                  context:NULL];
    }
    
}


// Baja
-(void)tearDownKVO {
    
    
    
//    for (NSString *key in [JMFNameEntry observableKeys]) {
   for (NSString *key in [self.class observableKeys]) {  // Para que escuche la clase adecuada: 'Notebook'
       [self removeObserver:self
                  forKeyPath:key];
         
    }
    
}


// El valor ha cambiado
-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    self.modificationDate = [NSDate date];
}


@end
