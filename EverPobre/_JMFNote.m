// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFNote.m instead.

#import "_JMFNote.h"

const struct JMFNoteAttributes JMFNoteAttributes = {
	.text = @"text",
};

const struct JMFNoteRelationships JMFNoteRelationships = {
	.notebook = @"notebook",
	.photo = @"photo",
};

const struct JMFNoteFetchedProperties JMFNoteFetchedProperties = {
};

@implementation JMFNoteID
@end

@implementation _JMFNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Note";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Note" inManagedObjectContext:moc_];
}

- (JMFNoteID*)objectID {
	return (JMFNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic text;






@dynamic notebook;

	

@dynamic photo;

	






@end
