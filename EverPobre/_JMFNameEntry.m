// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFNameEntry.m instead.

#import "_JMFNameEntry.h"

const struct JMFNameEntryAttributes JMFNameEntryAttributes = {
	.creationDate = @"creationDate",
	.modificationDate = @"modificationDate",
	.name = @"name",
};

const struct JMFNameEntryRelationships JMFNameEntryRelationships = {
};

const struct JMFNameEntryFetchedProperties JMFNameEntryFetchedProperties = {
};

@implementation JMFNameEntryID
@end

@implementation _JMFNameEntry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"NamedEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"NamedEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"NamedEntry" inManagedObjectContext:moc_];
}

- (JMFNameEntryID*)objectID {
	return (JMFNameEntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic creationDate;






@dynamic modificationDate;






@dynamic name;











@end
