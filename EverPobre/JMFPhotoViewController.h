//
//  JMFPhotoViewController.h
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 17/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * PARA EVITAR BUCLES DE LLAMADAS ENTRE #import "...h"
 * (Necesario #import "JMFNote.h" en le fichero JMFPhotoViewController.h
 */

//#import "JMFNote.h"
@class JMFNote;

@interface JMFPhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic,strong) JMFNote *model;

@property (nonatomic, strong) JMFNote *note;

- (IBAction)takePhoto:(id)sender;
- (IBAction)apllyFilters:(id)sender;
- (IBAction)deleteImage:(id)sender;

-(id) initWithModel:(JMFNote *) model;

@end
