//
//  JMFNoteViewController.h
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 17/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>


@class JMFNote;

@interface JMFNoteViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nombreView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)displayPhoto:(id)sender;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarView;

-(IBAction)removeKyeboard:(id)sender;
-(id)initWithModel:(JMFNote *)model;


@end
