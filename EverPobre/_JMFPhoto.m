// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFPhoto.m instead.

#import "_JMFPhoto.h"

const struct JMFPhotoAttributes JMFPhotoAttributes = {
	.photoData = @"photoData",
};

const struct JMFPhotoRelationships JMFPhotoRelationships = {
	.note = @"note",
};

const struct JMFPhotoFetchedProperties JMFPhotoFetchedProperties = {
};

@implementation JMFPhotoID
@end

@implementation _JMFPhoto

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Photo";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:moc_];
}

- (JMFPhotoID*)objectID {
	return (JMFPhotoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic photoData;






@dynamic note;

	






@end
