//
//  JMFPhotoViewController.m
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 17/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFPhotoViewController.h"
#import "JMFNote.h"
#import "JMFPhoto.h"
#import "JMFPhotoViewController.h"

@interface JMFPhotoViewController ()

@end

@implementation JMFPhotoViewController

#pragma mark - init
-(id) initWithModel:(JMFNote *) model {
    if (self == [super initWithNibName:nil bundle:nil]) {
        _model = model;
        self.title = model.name;
    }
    
    return self;
}


#pragma mark - view life cyle
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.imageView.image = self.model.photo.image;  // Core data se encarga de haceder a photo.
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    /* -----------------------------------------
     *
     * Código del modelo en el controlador !!!
     *
     * ( En el momento en que serepite código 
     * es por esto)
     *
     -------------------------------------------*/
    
//    NSManagedObjectContext *moc = self.model.managedObjectContext;
//    
//    // Borrar la antigua
//    [moc deleteObject:self.model.photo];
//    
    JMFPhoto *newPhoto = [JMFPhoto photoForNote:self.model
                                       inCotext:self.model.managedObjectContext];
    
    newPhoto.image = self.imageView.image;
    
}

#pragma mark - actions
- (IBAction)takePhoto:(id)sender {
}

- (IBAction)apllyFilters:(id)sender {
}

- (IBAction)deleteImage:(id)sender {
    
    // Borra del modelo
    self.model.photo = nil;
    
    // Borrar de la lista
//    self.imageView.image = nil;
    
    // Animacion
    CGRect oldBounds = self.imageView.bounds;
    [UIView animateWithDuration:0.8
                          delay:0
                        options:0
                     animations:^{
                         self.imageView.bounds = CGRectZero;
                         self.imageView.alpha = 0;
                         self.imageView.transform = CGAffineTransformMakeRotation(M_2_PI);
                         
                     } completion:^(BOOL finished) {
                         self.imageView.image = nil;
                         self.imageView.bounds = oldBounds;
                         self.imageView.alpha = 1;
                     }];
     }


@end
