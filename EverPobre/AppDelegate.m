//
//  AppDelegate.m
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 11/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "AppDelegate.h"
#import "JMFNote.h"
#import "JMFNotebook.h"
#import "JMFNotebookViewController.h"
#import "UIViewController+Navigator.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
//    self.model = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];


    
    self.model = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
//    [self trastearConDatos];
    
    
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[JMFNotebook entityName]];
//    req.sortDescriptors = @[[NSSortDescriptor
//                             sortDescriptorWithKey:JMFNameEntryAttributes.name
//                             ascending:YES],
//                            [NSSortDescriptor
//                             sortDescriptorWithKey:JMFNameEntryAttributes.modificationDate
//                             ascending:NO]];
    
    req.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:JMFNameEntryAttributes.name
                                                          ascending:YES
                                                           selector:@selector(caseInsensitiveCompare:)], // Para que no ordena las minusculas despues de la mayuscualas.
                                                        [NSSortDescriptor
                                                         sortDescriptorWithKey:JMFNameEntryAttributes.modificationDate
                                                         ascending:NO]];

    
    NSFetchedResultsController *fetched = [[NSFetchedResultsController alloc]
                                           initWithFetchRequest:req
                                           managedObjectContext:self.model.context
                                           sectionNameKeyPath:nil
                                           cacheName:nil];
    
    
//    AGTCoreDataTableViewController *nbVC = [[AGTCoreDataTableViewController alloc]
    JMFNotebookViewController *nbVC = [[JMFNotebookViewController alloc]
                                            initWithFetchedResultsController:fetched
                                            style:UITableViewStylePlain];
    
    self.window.rootViewController = [nbVC wrappedInNavigator];

    [self autoSave];
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
   // Guardar datos de la base de datos
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error guardando \n%@", error);
    }];

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /* 
     * No se guarda dos veces porque  simepre pregunta si hay algo que guardar.
     */
    // Guardar datos de la base de datos
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error guardando \n%@", error);
    }];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"No hay tiempo para guardar nada");
}


-(void) application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    
}

-(void) trastearConDatos { 
    
    
    // Crea libreta
//    JMFNotebook *nb = [NSEntityDescription
//                       insertNewObjectForEntityForName:@"Notebook" inManagedObjectContext:self.model.context];
    
//    JMFNotebook *nb = [JMFNotebook notebookWithName:@"Ex-Novias para el recuerdo" inConntext:self.model.context];
    
    JMFNotebook *nb = [JMFNotebook notebookWithName:@"Ex-Novias para el recuerdo"  inContext:self.model.context];
    
//    nb.name = @"Ex-novias para el recuerdo";
//    nb.creationDate = [NSDate date];
//    nb.modificationDate = [NSDate date];
    
    // Crea nota
//    JMFNote *n = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:self.model.context];
    JMFNote *n = [JMFNote noteWithName: @"Camila" notebook:nb inContext:self.model.context];
    
//    n.name  = @"Camila";
//    n.text = @"A esta le quedan 3";
//    n.creationDate = [NSDate date];
//    n.modificationDate = [NSDate date];
    
//    n.notebook = nb;
    
    NSLog(@"%@",n);

    NSLog(@"%@",n.modificationDate);
    n.name = @"Andriana Lima";
    NSLog(@"%@",n.modificationDate);
    
    
    // Buscar
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[JMFNote entityName]];
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:JMFNameEntryAttributes.name
                             ascending:YES],
                             [NSSortDescriptor
                             sortDescriptorWithKey:JMFNameEntryAttributes.modificationDate
                              ascending:NO]];
    
    NSError * error = nil;
    NSArray *results = [self.model.context executeFetchRequest:req
                                                         error:&error];
    
    if (results == nil) {
        // La cagamos
        NSLog(@"La cagamos al buscar mifran %@", error);
        
    }
    else {
        NSLog(@"Notas: %@", results);
    }
    
    // Guardar
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"la cagamos al guardar %@", error);
    }];
    
}

-(void) autoSave {
    
    if (AUTO_SAVE) {
        NSLog(@"AUTO GUARDANDO .. ");
  
    
    // guarda
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error guardando \n%@", error);
    }];
    
    // Llamada cada 5 segundos
    [self performSelector:@selector(autoSave)
               withObject:nil
               afterDelay:AUTO_SAVE_DELAY];
    
    }
}

@end
