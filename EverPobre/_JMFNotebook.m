// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFNotebook.m instead.

#import "_JMFNotebook.h"

const struct JMFNotebookAttributes JMFNotebookAttributes = {
};

const struct JMFNotebookRelationships JMFNotebookRelationships = {
	.notes = @"notes",
};

const struct JMFNotebookFetchedProperties JMFNotebookFetchedProperties = {
};

@implementation JMFNotebookID
@end

@implementation _JMFNotebook

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Notebook" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Notebook";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Notebook" inManagedObjectContext:moc_];
}

- (JMFNotebookID*)objectID {
	return (JMFNotebookID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic notes;

	
- (NSMutableSet*)notesSet {
	[self willAccessValueForKey:@"notes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"notes"];
  
	[self didAccessValueForKey:@"notes"];
	return result;
}
	






@end
