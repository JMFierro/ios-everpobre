#import "JMFNotebook.h"


@interface JMFNotebook ()

+(NSArray *)observableKeys;
    
    
@end


@implementation JMFNotebook

+(NSArray *)observableKeys {
    return @[JMFNameEntryAttributes.name,
             JMFNameEntryAttributes.creationDate,
             JMFNotebookRelationships.notes];
    
    
}


+(instancetype) notebookWithName:(NSString *)name
                       inContext: (NSManagedObjectContext *) context {
    
    
    JMFNotebook *nb = [JMFNotebook insertInManagedObjectContext:context];
    
    nb.name = name;
    nb.creationDate = [NSDate date];
    nb.modificationDate = [NSDate date];
    
    return nb;
}

#pragma mark - 

@end
