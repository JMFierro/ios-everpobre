//
//  JMFNoteViewController.m
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 17/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFNoteViewController.h"
#import "JMFNote.h"
#import "JMFPhotoViewController.h"

@interface JMFNoteViewController ()
@property (nonatomic,strong) JMFNote *model;
@property (nonatomic) CGRect oldRect;

@end

@implementation JMFNoteViewController


#pragma mark - View Life cycle
-(id)initWithModel:(JMFNote *)model {
    
    if (self = [super initWithNibName:nil bundle:nil]){
        _model = model;
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Asigna delegado
    self.nombreView.delegate = self;
    
    // Deja espacio para la barra Navigation.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // sincronizar vista con modelo
    self.nombreView.text = self.model.name;
    self.textView.text = self.model.text;
    self.title  = self.model.name;
    
    // ALTA EN NOTIFICAIONES
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(notifyKeyboardWillApper:)
               name:UIKeyboardWillShowNotification object:nil];
    
    
    [nc addObserver:self
           selector:@selector(notifyKeyboardWillDisappear:)
                        name:UIKeyboardWillHideNotification
             object:nil];
    
    // Configuracion de una barra de teclado para la textView
    UIBarButtonItem *hideKeyboard = [[UIBarButtonItem alloc]
                                     initWithTitle:@"Hide"
                                     style:UIBarButtonItemStylePlain
                                     target:self action:@selector(removeKeyboard:)];
 
    UIBarButtonItem *displayTextField = [[UIBarButtonItem alloc]
                                         initWithTitle:@"Name" style:UIBarButtonItemStylePlain target:self action:@selector(displayName:)];
    
    UIToolbar *keyboardToolbar = [[UIToolbar alloc] init];
    keyboardToolbar.items = @[hideKeyboard, displayTextField];
    self.textView.inputAccessoryView = keyboardToolbar;
    
//    keyboardToolbar.barStyle = UIBarStyleBlackOpaque;
//    keyboardToolbar.translucent = NO;
    
//    keyboardToolbar.barStyle = UIBarStyleDefault;
//    keyboardToolbar.translucent = NO;
//    keyboardToolbar.barTintColor = [UIColor orangeColor];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.model.text =self.nombreView.text;
    self.model.name = self.textView.text;
    
    // Baja de notificaciones
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/* -----------------------------------------------------------
 *
 * RECONMENDACION: poner de quie se recibe la notificación:
 *
 -------------------------------------------------------------*/

#pragma mark - Notificaciones
// UIKeyboardWillShowNotification
-(void) notifyKeyboardWillApper:(NSNotification *) notification {
    
    // Obtener el franme del teclado
    NSDictionary *info = notification.userInfo;
    NSValue *keyFrameValeu = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyFrame = [keyFrameValeu CGRectValue];  // tamaño del teclado
    
    
    // La duracion del teclado
    double duration  =[[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // Nuevo CGRect
    self.oldRect = self.textView.frame;
    CGRect newRect = CGRectMake (self.oldRect.origin.x,
                                 self.oldRect.origin.y,
                                 self.oldRect.size.width,
                                 self.oldRect.size.height - keyFrame.size.height + self.toolbarView.frame.size.height );
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         self.textView.frame = newRect;
                     } completion:^(BOOL finished) {
                         //
                     }];
}

// UIKeyboardWillHideNotification
-(void) notifyKeyboardWillDisappear:(NSNotification *) notification {
    
    // Obtener el franme del teclado
    NSDictionary *info = notification.userInfo;
    
    
    // La duracion del teclado
    double duration  =[[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // Nuevo CGRect
//    self.oldRect = self.textView.frame;
//    CGRect newRect = CGRectMake (self.oldRect.origin.x,
//                                 self.oldRect.origin.y,
//                                 self.oldRect.size.width,
//                                 self.oldRect.size.height - keyFrame.size.height + self.toolbarView.frame.size.height );
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         self.textView.frame = self.oldRect;
                     } completion:^(BOOL finished) {
                         //
                     }];

    
}


#pragma mark - actions

-(void) dispplayName:(id) sender {
  
    [self.nombreView becomeFirstResponder];
    
}

- (IBAction)displayPhoto:(id)sender {
    
    JMFPhotoViewController *pVC = [[JMFPhotoViewController alloc] initWithModel:self.model];
    
    // hago el push
    [self.navigationController pushViewController:pVC animated:YES];
}

-(IBAction)removeKyeboard:(id)sender {
    
    // Liberar el teclado cualquiera que lo tenga.
    [self.view endEditing:YES];
    
}

#pragma mark - UITextFieldDelegate
-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    // Comprpbar que el texto es correcto.
    
    
    // ocultar el teclado.
    [textField resignFirstResponder];
    
    
    // devuelve YES/NO según el conenido del texto.
    return YES;
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //  Si llega hasta aquí el texto esta bien, se puede copiar.
    NSLog(@"He terminado de editar");
    
}

@end
