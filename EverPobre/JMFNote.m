#import "JMFNote.h"
#import "JMFPhoto.h"


@interface JMFNote ()

+(NSArray *)observableKeys;

@end


@implementation JMFNote

#pragma mark - Properties
-(void) setPhoto: (JMFPhoto *) newPhoto {
    
    if (self.photo != nil) {

        // Pilla el moc (contexto)
        NSManagedObjectContext *moc = self.managedObjectContext;
        
        // Matar JMFPhoto huerfana
        [moc deleteObject:self.photo];
        
    }
    
        
        
    // guardar el nuevo en Core Data
//    _photo = newPhoto;                        // NO
    
    // ** KVO **
//    [self setValue:newPhoto
//          forKey:JMFNoteRelationships.photo]; // NO Bucle: 'setValue te vuelve a a meter en setPhoto

    // Aviso de cambio va a cambiar la propiedad (setPrimiteValue: No lo hace
    [self willChangeValueForKey:JMFNoteRelationships.photo];
    
    // ** KVO ** los primitive no crean los setters: hay que enviarla
    [self setPrimitiveValue:newPhoto forKey:JMFNoteRelationships.photo];
    
    // Aviso de que ya lo ha cambiado
    [self didChangeValueForKey:JMFNoteRelationships.photo];
    
}




+(NSArray *)observableKeys {
    return @[JMFNameEntryAttributes.name,
             JMFNameEntryAttributes.creationDate,
//             JMFNameEntryAttributes.modificationDate,
             JMFNoteAttributes.text,
             JMFNoteRelationships.photo,
             @"photo.photoData"];
//             [NSString stringWithFormat:@"%@.%@", JMFPhotoRelationships.photo,
//              JMFPhotoAttributes.photo.photoDate];
    
    
}


+(instancetype) noteWithName:(NSString *) name 
                    notebook:(JMFNotebook *) notebook
                   inContext:(NSManagedObjectContext *)context {
    
    JMFNote *n = [JMFNote insertInManagedObjectContext:context];
    n.name = name;
    n.notebook = notebook;
    n.creationDate = [NSDate date];
    n.modificationDate = [NSDate date];
    
    return n;
}
@end
