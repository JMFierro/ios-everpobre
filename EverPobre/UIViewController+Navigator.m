//
//  UIViewController+Navigator.m
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 13/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "UIViewController+Navigator.h"

@implementation UIViewController (Navigator)

-(UINavigationController *)wrappedInNavigator {
    UINavigationController *navVC = [[UINavigationController alloc] init];
    [navVC pushViewController:self
                     animated:NO];
    return navVC;
}

@end
