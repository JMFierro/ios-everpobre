//
//  JMFNotesViewController.h
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 14/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"
#import "JMFNotebook.h"

@interface JMFNotesViewController : AGTCoreDataTableViewController


-(id) initWithNotebook:(JMFNotebook *) notebook
fetchedResultController:(NSFetchedResultsController *) controller
                 style:(UITableViewStyle) style;
@end
