// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFNotebook.h instead.

#import <CoreData/CoreData.h>
#import "JMFNameEntry.h"

extern const struct JMFNotebookAttributes {
} JMFNotebookAttributes;

extern const struct JMFNotebookRelationships {
	__unsafe_unretained NSString *notes;
} JMFNotebookRelationships;

extern const struct JMFNotebookFetchedProperties {
} JMFNotebookFetchedProperties;

@class JMFNote;


@interface JMFNotebookID : NSManagedObjectID {}
@end

@interface _JMFNotebook : JMFNameEntry {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (JMFNotebookID*)objectID;





@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;





@end

@interface _JMFNotebook (CoreDataGeneratedAccessors)

- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(JMFNote*)value_;
- (void)removeNotesObject:(JMFNote*)value_;

@end

@interface _JMFNotebook (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;


@end
