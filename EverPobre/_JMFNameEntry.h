// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFNameEntry.h instead.

#import <CoreData/CoreData.h>


extern const struct JMFNameEntryAttributes {
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *modificationDate;
	__unsafe_unretained NSString *name;
} JMFNameEntryAttributes;

extern const struct JMFNameEntryRelationships {
} JMFNameEntryRelationships;

extern const struct JMFNameEntryFetchedProperties {
} JMFNameEntryFetchedProperties;






@interface JMFNameEntryID : NSManagedObjectID {}
@end

@interface _JMFNameEntry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (JMFNameEntryID*)objectID;





@property (nonatomic, strong) NSDate* creationDate;



//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* modificationDate;



//- (BOOL)validateModificationDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;






@end

@interface _JMFNameEntry (CoreDataGeneratedAccessors)

@end

@interface _JMFNameEntry (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;




- (NSDate*)primitiveModificationDate;
- (void)setPrimitiveModificationDate:(NSDate*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




@end
