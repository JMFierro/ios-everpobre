//
//  JMFNotesViewController.m
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 14/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFNotesViewController.h"
#import "JMFNote.h"
#import "JMFPhoto.h"
#import "JMFNoteViewController.h"

@interface JMFNotesViewController ()
@property (nonatomic,strong) JMFNotebook *notebook;

@end

@implementation JMFNotesViewController

-(id) initWithNotebook:(JMFNotebook *) notebook
fetchedResultController:(NSFetchedResultsController *) controller
                 style:(UITableViewStyle) style{
    
    if (self = [super initWithFetchedResultsController:controller style:style]) {
        _notebook = notebook;
        self.title = notebook.name;
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                            target:self
                            action:@selector(addNote:)];
    
    self.navigationItem.rightBarButtonItem = btn;
}

-(void) addNote:(id) sender {
    
//    [JMFNote noteWithName:@"Nueva nota"
//                 notebook:self.notebook
//                inContext:self.notebook.managedObjectContext];
//    
    JMFNote *n = [JMFNote noteWithName:@"Nueva nota"
                 notebook:self.notebook
                inContext:self.notebook.managedObjectContext];
    n.photo = [JMFPhoto photoForNote:n
                            inCotext:n.managedObjectContext];
    n.photo.image = [UIImage imageNamed:@"mexico-escapada-paisaje-lagos-lagunas-destinos-rios-sol-horizonte.jpg"];
    
    n.text = @"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.";
    
}


#pragma mark - dat source
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    // la nota
    JMFNote *note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // la celda
    static NSString *cellid = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    
    // sincronizarlos
    cell.imageView.image = note.photo.image;
    cell.textLabel.text = note.name;
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateStyle = NSDateFormatterMediumStyle;
    cell.detailTextLabel.text = [fmt stringFromDate:note.modificationDate];
    
    // devolver
    return cell;
    
    
}


#pragma marck - delegado
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Averiguar la nota
    JMFNote *note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear un NoteVC
    JMFNoteViewController *noteVC = [[JMFNoteViewController alloc] initWithModel:note];
    
    // Hacer el push
    [self.navigationController pushViewController:noteVC animated:YES];
}
@end
