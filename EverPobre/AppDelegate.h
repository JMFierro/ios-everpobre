//
//  AppDelegate.h
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 11/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTCoreDataStack.h"

//#define AUTO_SAVE boolean *ua

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) AGTCoreDataStack *model;



@end
