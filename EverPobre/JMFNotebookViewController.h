//
//  JMFNotebookViewController.h
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 13/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"

@interface JMFNotebookViewController : AGTCoreDataTableViewController

@end
