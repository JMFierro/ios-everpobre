#import "_JMFNote.h"

@interface JMFNote : _JMFNote {}
+(instancetype) noteWithName:(NSString *) name 
                    notebook:(JMFNotebook *) notebook
                   inContext:(NSManagedObjectContext *)context;
@end
