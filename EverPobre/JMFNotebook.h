#import "_JMFNotebook.h"

@interface JMFNotebook : _JMFNotebook {}


+(instancetype) notebookWithName:(NSString *)name
                       inContext: (NSManagedObjectContext *) context;

// Custom logic goes here.
@end
