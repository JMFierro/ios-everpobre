#import "JMFPhoto.h"


@interface JMFPhoto ()

// Private interface goes here.

@end


@implementation JMFPhoto

#pragma mark - Properties 
// Por heredar de manager hay que crear los getters y setters.
// (Tambien se puede hacer con '@dinamic' si se va a hacer algo con código.)
@synthesize image=_image;

-(UIImage *) image {
    return [UIImage imageWithData:self.photoData];
}

-(void) setImage:(UIImage *)image {
    self.photoData = UIImageJPEGRepresentation(image, 0.9);  // 0.9 es la calidad con que guardarla.
}

+(instancetype) photoForNote:(JMFNote *)note
                    inCotext:(NSManagedObjectContext *) context {
    
    JMFPhoto *photo = [JMFPhoto insertInManagedObjectContext:context];
    photo.note = note;
    
    return photo;
}

@end
