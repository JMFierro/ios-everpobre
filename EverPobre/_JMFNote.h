// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFNote.h instead.

#import <CoreData/CoreData.h>
#import "JMFNameEntry.h"

extern const struct JMFNoteAttributes {
	__unsafe_unretained NSString *text;
} JMFNoteAttributes;

extern const struct JMFNoteRelationships {
	__unsafe_unretained NSString *notebook;
	__unsafe_unretained NSString *photo;
} JMFNoteRelationships;

extern const struct JMFNoteFetchedProperties {
} JMFNoteFetchedProperties;

@class JMFNotebook;
@class JMFPhoto;



@interface JMFNoteID : NSManagedObjectID {}
@end

@interface _JMFNote : JMFNameEntry {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (JMFNoteID*)objectID;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) JMFNotebook *notebook;

//- (BOOL)validateNotebook:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) JMFPhoto *photo;

//- (BOOL)validatePhoto:(id*)value_ error:(NSError**)error_;





@end

@interface _JMFNote (CoreDataGeneratedAccessors)

@end

@interface _JMFNote (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;





- (JMFNotebook*)primitiveNotebook;
- (void)setPrimitiveNotebook:(JMFNotebook*)value;



- (JMFPhoto*)primitivePhoto;
- (void)setPrimitivePhoto:(JMFPhoto*)value;


@end
