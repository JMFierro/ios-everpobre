#import "_JMFPhoto.h"

@interface JMFPhoto : _JMFPhoto {}


@property (strong, nonatomic) UIImage *image;

+(instancetype) photoForNote:(JMFNote *)note
                    inCotext:(NSManagedObjectContext *) context;

@end

