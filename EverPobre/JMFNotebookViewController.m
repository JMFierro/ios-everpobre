//
//  JMFNotebookViewController.m
//  EverPobre
//
//  Created by José Manuel Fierro Conchouso on 13/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFNotebookViewController.h"
#import "JMFNotebook.h"
#import "JMFNote.h"
#import "JMFNotesViewController.h"

@interface JMFNotebookViewController ()

@end

@implementation JMFNotebookViewController

-(NSString *) title{
    return @"Everpobre";
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIBarButtonItem *add = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                            target:self
                            action:@selector(addNotebook:)];
    
    self.navigationItem.rightBarButtonItem = add;
}

-(void)addNotebook:(id) sender {
    static NSInteger i = 0;
    i = i +1;
    NSString *string = [NSString stringWithFormat:@"Elemento (id: %li)", (long)i];
    JMFNotebook *newNb = [JMFNotebook notebookWithName:string                                             inContext:self.fetchedResultsController.managedObjectContext];
    
//    JMFNote *note = [JMFNote noteWithName:@"Nota 1" notebook:newNb inContext:self.fetchedResultsController.managedObjectContext];
//    JMFNote *note2 = [JMFNote noteWithName:@"Nota 2" notebook:newNb inContext:self.fetchedResultsController.managedObjectContext];
//    JMFNote *note3= [JMFNote noteWithName:@"Nota 3" notebook:newNb inContext:self.fetchedResultsController.managedObjectContext];
//    JMFNote *note4= [JMFNote noteWithName:@"Nota 4" notebook:newNb inContext:self.fetchedResultsController.managedObjectContext];
//    JMFNote *note5= [JMFNote noteWithName:@"Nota 5" notebook:newNb inContext:self.fetchedResultsController.managedObjectContext];
    
    NSLog(@"Una nueva libreta: %@", newNb);
}

#pragma mark - Data source
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    // Averiguar la libreta
    JMFNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Pedir celda o crearla
    static NSString *cellId = @"celda";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
        initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellId];
        
    }
    
  
    // Sincronizamos modelo y vista (libreta y celda)
      cell.textLabel.text = nb.name;
    
    // Devolvemos
    return cell;
}

#pragma mark - delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // la libreta
    JMFNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[JMFNote entityName]];
    
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:JMFNameEntryAttributes.name
                             ascending:NO],
                            [NSSortDescriptor
                             sortDescriptorWithKey:JMFNameEntryAttributes.name
                             ascending:YES
                             selector:@selector(caseInsensitiveCompare:)]];
    
    // toda  nota tiene que cumplir: @"self.notebook = %@", nb
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.notebook = %@", nb];
    req.predicate = pred;
    
    // fetchedresults
    NSFetchedResultsController *ctrl = [[NSFetchedResultsController alloc]
                                        initWithFetchRequest:req
                                        managedObjectContext:nb.managedObjectContext
                                        sectionNameKeyPath:nil
                                        cacheName:nil];

    
    
    // conrolador
    JMFNotesViewController *notesVC = [[JMFNotesViewController alloc]
                                       initWithNotebook:nb
                                       fetchedResultController:ctrl
                                       style:UITableViewStylePlain];
    
    // Push
    [self.navigationController pushViewController:notesVC animated:YES];
    
}

@end
