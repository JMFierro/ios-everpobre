// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to JMFPhoto.h instead.

#import <CoreData/CoreData.h>


extern const struct JMFPhotoAttributes {
	__unsafe_unretained NSString *photoData;
} JMFPhotoAttributes;

extern const struct JMFPhotoRelationships {
	__unsafe_unretained NSString *note;
} JMFPhotoRelationships;

extern const struct JMFPhotoFetchedProperties {
} JMFPhotoFetchedProperties;

@class JMFNote;



@interface JMFPhotoID : NSManagedObjectID {}
@end

@interface _JMFPhoto : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (JMFPhotoID*)objectID;





@property (nonatomic, strong) NSData* photoData;



//- (BOOL)validatePhotoData:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) JMFNote *note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;





@end

@interface _JMFPhoto (CoreDataGeneratedAccessors)

@end

@interface _JMFPhoto (CoreDataGeneratedPrimitiveAccessors)


- (NSData*)primitivePhotoData;
- (void)setPrimitivePhotoData:(NSData*)value;





- (JMFNote*)primitiveNote;
- (void)setPrimitiveNote:(JMFNote*)value;


@end
